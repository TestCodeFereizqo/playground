import Cocoa

//func fibonacciSequence (n: Int) -> [Int]  {
//    var fibonacciArray = [Int]()
//    for n in 0 ... n {
//
//        if n == 0 {
//            fibonacciArray.append(0)
//        }
//        else if n == 1 {
//            fibonacciArray.append(1)
//        }
//        else {
//            fibonacciArray.append (fibonacciArray[n-1] + fibonacciArray[n-2] )
//        }
//    }
//    return fibonacciArray
//}

//func makeFibonacci(count:Int) -> [Int]{
//    var fibonacciArray: [Int] = []
//    for i in 0...count {
//        if i == 0 {
//            fibonacciArray.append(0)
//        } else if i == 1 {
//            fibonacciArray.append(1)
//        }
//        else {
//            fibonacciArray.append(fibonacciArray[i-1] + fibonacciArray[i-2])
//        }
//    }
//    fibonacciArray.remove(at: 0)
//    return fibonacciArray
//}
//
////let digits = string.compactMap{ $0.wholeNumberValue }
////if consonants.contains(char)
//
//func minMaxFibonacciNumber(input: [Int]) -> [Int] {
//    // Enter your code here.
//    var fibonacci = makeFibonacci(count: input.count)
//    var maxFibo = 0
//    var minFibo = 0
//    var num: [Int] = []
//    for i in input {
//        if fibonacci.contains(i) {
//            fibonacci.append(fibonacci[i-1])
//        }
//    }
//
//    print(num)
//    return [minFibo, maxFibo]
//}
//
//let array = [1,25,13,22,21,24,23]
//minMaxFibonacciNumber(input: array)


//func pageCount(n: Int, p: Int) -> Int {
//    var bookTurn = 0
//    var pageTurn = 0
//    // Detect book turn
//    // case genap
//    if n % 2 == 0 {
//        bookTurn = n/2
//    }
//    // case ganjil
//    else {
//        bookTurn = (n-1)/2
//    }
//
//    // Detect page turn
//    // case genap
//    if p % 2 == 0 {
//        pageTurn = p/2
//    }
//    // case ganjil
//    else {
//        pageTurn = (p-1)/2
//    }
//
//    let lastTurn = bookTurn - pageTurn
//
//    return min(pageTurn, lastTurn)
//}
//
//let n = 6
//let p = 2
//pageCount(n: n, p: p)

//func countingValleys(n: Int, s: String) -> Int {
//    var altitude = 0
//    var countValley = 0
//
//    for hike in s {
//        if hike == "U" {
//            altitude += 1
//            if altitude == 0 {
//                countValley += 1
//            }
//        } else if hike == "D" {
//            altitude -= 1
//        }
//    }
//
//    return countValley
//}
//
//let n = 8
//let hikes = "UDDDUDUU"
//
//countingValleys(n: n, s: hikes)
//
//func getMoneySpent(keyboards: [Int], drives: [Int], b: Int) -> Int {
//    var spendMoney = 0
//    var maxSpend = 0
//
//    for key in keyboards {
//        for drive in drives {
//            spendMoney = key + drive
//            if spendMoney < b  && spendMoney > maxSpend {
//                maxSpend = spendMoney
//            }
//        }
//    }
//
//    if maxSpend == 0 {
//        maxSpend = -1
//    }
//
//    return maxSpend
//}
//
//let keyboard = [10]
//let drives = [10]
//
//getMoneySpent(keyboards: keyboard, drives: drives, b: 10)
//
//var A = 7
//A += 5

// Dependency Injection

/// Without
//class Cat {
//    let name: String
//
//    init(name: String) {
//        self.name = name
//    }
//
//    func Sound() -> String {
//        return "Meow!"
//    }
//}
//
//class PetOwner {
//    let pet = Cat(name: "Meo")
//
//    func play() -> String {
//        return "I'm playing with \(pet.name). \(pet.Sound()) "
//    }
//}
//
//let adi = PetOwner()
//adi.play()

///// Using
//protocol AnimalType {
//    var name: String { get }
//    func Sound() -> String
//}
//
//class Cat: AnimalType {
//    var name: String
//
//    init(name: String) {
//        self.name = name
//    }
//
//    func Sound() -> String {
//        return "Meow!"
//    }
//}
//
//class Dog: AnimalType {
//    var name: String
//
//    init(name: String) {
//        self.name = name
//    }
//
//    func Sound() -> String {
//        return "Bow!"
//    }
//
//
//}
//
//class PetOwner {
//    let pet: AnimalType
//
//    init(pet: AnimalType) {
//        self.pet = pet
//    }
//
//    func play() -> String {
//        return "I'm playing with \(pet.name). \(pet.Sound())"
//    }
//}
//
//let adi = PetOwner(pet: Cat(name: "Mini"))
//adi.play()
//let ida = PetOwner(pet: Dog(name: "Mono"))
//ida.play()


/// Using Swinject


func foo (x: [Int], a: Int, b: Int, i: Int, j: Int) {
    var k = j
    var ct = 0
    
    while k > i-1 {
        if x[k] <= b && x[k] <= a {
            ct = ct + 1
        }
    }
}

import Cocoa

let test = "Test"

func detectFirstCharacter(word: String) {
    let firstChar = word.prefix(1)
    let testChar = word.prefix(1)
    if firstChar == testChar.lowercased() {
        print(firstChar)
    } else if firstChar == testChar.uppercased() {
        print(firstChar)
    } else if firstChar == "" {
        print(firstChar)
    } else {
        print("Other")
    }
}

let a = detectFirstCharacter(word: test)

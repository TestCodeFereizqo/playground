import UIKit

//// 1 - Simple func
//func sayHi() {
//    print("Hello there...")
//}
//sayHi()

//// 2 - Function with param and no return
//func sayMyName(name:String) {
//    print("Say my name is \(name)")
//}
//sayMyName(name: "Boom")

//// 3 - When we have return, we need to indicate the type with ->
//func multiply(number1: Int, number2: Int) -> Int {
//    let result = number1*number2
//    return result
//}
//multiply(number1: 4, number2: 10)

// 4 - Function without parameter but with return
//func generatorGacha() -> Int {
//    let generatedNumber:Int = Int.random(in: 1...999)
//    return generatedNumber
//}
//print(generatorGacha())
//print("---")
//for _ in 1...10{
//    print(generatorGacha())
//}

//func generatorGachaa() {
//    let _:Int = Int.random(in: 1...999)
//}
//print(generatorGachaa())

func celciusToKelvin(celcius:Double) -> Double {
    let kelvin = celcius+273
    return kelvin
}

func celciusToFahrenheit(celcius:Double) -> Double {
    let fahrenheit = celcius/9*5-32
    return fahrenheit
}
let celcius = 10.0

print("celcius = \(celcius) ºC \nkelvin = \(celciusToKelvin(celcius: celcius)) K \nfahrenheit \(celciusToFahrenheit(celcius: celcius)) ºF")

func multipy(a: Int, b: Int) -> Int {
    return a*b*a
}

func multiple (firstValue: Int, secondValue: Int) {
    let result = firstValue*secondValue
    print(result)
}
let ade = multiple(firstValue: 2, secondValue: 3)

func multiplee (firstValue: Int, secondValue: Int) -> Int {
    return firstValue*secondValue
}
let adee = multiplee(firstValue: 2, secondValue: 3)

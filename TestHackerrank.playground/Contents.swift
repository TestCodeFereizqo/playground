import Cocoa

import Foundation

//func compareTriplets(a: [Int], b: [Int]) -> [Int] {
//    var result = [0,0]
//    for i in 0...a.count-1 {
//        print(i)
//        if a[i] > b[i] {
//            result[0] += 1
//        } else if a[i] < b[i] {
//            result[1] += 1
//        } else if a[i] == b[i] {
//            result[0] += 0
//        }
//    }
//    return result
//}
//
//let a = [0,1,2]
//let b = [2,3,4]
//print(compareTriplets(a: [0,1], b: [9,3]))
//print(compareTriplets(a:a,b:b))

//func plusMinus(arr: [Int]) -> Void {
//    var plus: Int = 0
//    var minus: Int = 0
//    var zero: Int = 0
//
//    for i in arr {
//        if i > 0 {
//            plus += 1
//        } else if i < 0 {
//            minus += 1
//        } else if i == 0 {
//            zero += 1
//        }
//    }
//
//    let pluss = Double(plus)/Double(arr.count)
//    print(String(format: "%.6f", pluss))
//}

//func miniMaxSum(arr: [Int]) -> Void {
//    var except1 = -arr[0]
//    var except2 = -arr[1]
//    var except3 = -arr[2]
//    var except4 = -arr[3]
//    var except5 = -arr[4]
//
//    for i in arr {
//        except1 += i
//        except2 += i
//        except3 += i
//        except4 += i
//        except5 += i
//    }
//    let NumArray = [except1,except2,except3,except4,except5]
//    var Largest = 0
//    for i in NumArray {
//        Largest = max(Largest, i)
//    }
//
//    let minimum = min(except5, except4)
//    print(Largest)
//}

//func timeConversion(s: String) -> String {
//    /*
//     * Write your code here.
//     */
//
//    let time12 = s
//    var timeArray = time12.components(separatedBy: ":")
//    var hour = Int(timeArray[0])!
//    var minute = timeArray[1]
//    var timeSecond = timeArray[2]
//    var second = timeSecond.prefix(2)
//    var detect = timeSecond.suffix(2)
//
//    if detect == "PM" {
//        if hour == 12 {
//            hour = 12
//        } else {
//            hour += 12
//            if hour == 24 {
//                hour = 0
//            }
//        }
//
//    }
//
//    var hours = String(hour)
//    if hour < 10 {
//        hours = "0" + "\(hour)"
//    }
//
//    let time24 = String("\(hours):\(minute):\(second)")
//
//    return time24
//
//}
//
//let string = "12:50:51PM"
//timeConversion(s: string)

//let array = [1,2,3,4,5,6]
//
//let s = [1,1,1,1]
//let m = 3
//let d = 3
//
//let chunkSize = 2
//let chunks = stride(from: 0, to: array.count, by: 1).map {
//    Array(array[$0..<min($0 + chunkSize, array.count)])
//}
//print(chunks)
//
//print("--")
//
////let test = stride(from: 0, to: array.count, by: 1).map {
////    print("Test : \(array[$0...min($0 + 1, array.count)]) \n")
////}
//
//print("-----")
//
//let test2 = stride(from: 0, to: array.count, by: 1).map {
//    print("Test : \(array[$0..<min($0 + 2, array.count)]) \n")
//}
//
//print("----")
//
//let test3 = stride(from: 0, to: array.count, by: 1).map {
//    Array(array[$0..<min($0 + 2, array.count)])
//}
//print(test3)
//
//let count = test3.map{
//    $0.reduce(0, {$0+$1})
//}
//
//print(count)

//let array = [1,2,3,4,5,6]
//
//let m = 2
//let d = 3
//
//func bday(s: [Int], d: Int, m: Int) -> Int {
//    var count = 0
//
//    for i in 0...s.count-1{
//        var sum = 0
//        if i+m-1 < s.count-1 {
//            for j in 0...i+m-1 {
//                sum = sum + s[j]
//            }
//            if sum == d {count=count+1}
//        }
//    }
//    return count
//}

//func gradingStudents(grades: [Int]) -> [Int] {
//    var score: [Int] = []
//    for grade in grades {
//        if grade >= 38 && ((grade/5)+1)*5-grade < 3 {
//            score.append(((grade/5)+1)*5)
//        } else {
//            score.append(grade)
//        }
//    }
//    return score
//}
//
//let grade = [73,67,38,33]
//gradingStudents(grades: grade)


// s -t : titik rumah
// a : titik pohon apel
// b : titik pohon jeruk
// apple
// oranges

//func countApplesAndOranges(s: Int, t: Int, a: Int, b: Int, apples: [Int], oranges: [Int]) -> Void {
//    var countApple = 0
//    for apple in apples {
//        var appleLoc = 0
//        appleLoc = apple + a
//        if appleLoc >= s && appleLoc <= t {
//            countApple += 1
//        }
//    }
//
//    var countOrange = 0
//    for orange in oranges {
//        var orangeLoc = 0
//        orangeLoc = orange + b
//        if orangeLoc >= s && orangeLoc <= t {
//            countOrange += 1
//        }
//    }
//
//    print(countApple)
//    print(countOrange)
//}
//
//countApplesAndOranges(s: 7, t: 5, a: 3, b: -2, apples: [-2,2,1], oranges: [5,6])

//let a = [[3,5,7],[1,2,3],[3,1,5]]
//for i in 0...a.count-1 {
//    let b = a[i][i]
//    print(b)
//    let c = a[i][(a.count-1)-i]
//    print(c)
//}


//let n = 6
//for i in 0...n-1 {
//    for _ in 0...i {
//        print("i", terminator:"")
//    }
//    print("")
//}
//

//for i in 1...n {
//    let a = String(repeating: "i", count: i)
//    let b = String(repeating: " ", count: n-i)
//    print("\(b)\(a)")
//}

//let n = 14
//var a = 1
//for i in 1...14 {
//    a *= i
//}
//print(a)

//func extraLongFactorials(n: Int) -> Void {
//    var a = 1
//    for i in 1...n {
//        a *= i
//    }
//    a
//}
//
//extraLongFactorials(n: 25)

//let number = 18
//let formatter = NumberFormatter()
//formatter.numberStyle = .spellOut
//let numberString = formatter.string(from: NSNumber(value: number))

//func timeInWords(h: Int, m: Int) -> String {
//    var conjunction = ""
//    var minuteSentence = "minutes"
//
//    let formatter = NumberFormatter()
//    formatter.numberStyle = .spellOut
//    var hourString = formatter.string(from: NSNumber(value: h))
//    var minuteString = formatter.string(from: NSNumber(value: m))
//
//    switch m {
//    case 0...30:
//        conjunction = "past"
//        if m == 15 {
//            minuteSentence = ""
//            minuteString = "quarter"
//        }
//        if m == 30 {
//            minuteSentence = ""
//            minuteString = "half"
//        }
//        if m == 0 {
//            minuteSentence = ""
//            conjunction = ""
//            minuteString = ""
//            hourString = "\(hourString!) o' clock"
//        }
//
//    case 31...60:
//        conjunction = "to"
//        minuteString = formatter.string(from: NSNumber(value: 60-m))
//        hourString = formatter.string(from: NSNumber(value: h+1))
//        if h+1 == 13 {
//            hourString = formatter.string(from: NSNumber(value: 1))
//        }
//        if m == 45 {
//            minuteString = "quarter"
//            minuteSentence = ""
//        }
//    default:
//        break
//    }
//
//    minuteString = minuteString?.replacingOccurrences(of: "-", with: " ")
//
//    var timeInWord = "\(minuteString ?? "") \(minuteSentence) \(conjunction) \(hourString ?? "")"
//    timeInWord = timeInWord.replacingOccurrences(of: "  ", with: " ")
//    timeInWord = timeInWord.trimmingCharacters(in: .whitespaces)
//
//    print(timeInWord)
//    return timeInWord
//}

//timeInWords(h: 12, m: 43)

//let array = [1,2,4,5,6,7,7,7,7,8,8,8,6,6,5,5,4]
//var count = 1
//var condition = true
//
//for i in array {
//    if i > 5 && condition == true {
//        count += 1
//        condition = false
//    }
//}
//print(count)

//func kangaroo(x1: Int, v1: Int, x2: Int, v2: Int) -> String {
//    if v2 < v1 {
//        let selisihV = v1 - v2
//        let timeToX = x2/selisihV
//        let posisiPapas1 = timeToX*v1
//        let posisiPapas2 = timeToX*v2+x2
//
//        print(posisiPapas1)
//        print(posisiPapas2)
//
//        return "YES"
//    } else {
//        return "NO"
//    }
//}
//
//kangaroo(x1: 0, v1: 3, x2: 4, v2: 2)
//
//func kangaroo1(x1: Int, v1: Int, x2: Int, v2: Int) -> String {
//    let selisihV = v1 - v2
//    let timeToX = x2/selisihV
//    let posisiPapas1 = timeToX*v1
//    let posisiPapas2 = timeToX*v2+x2
//
//    print(posisiPapas1)
//    print(posisiPapas2)
//
//    if posisiPapas1 == posisiPapas2 {
//        return "YES"
//    } else {
//        return "NO"
//    }
//}
//
//kangaroo1(x1: 0, v1: 2, x2: 5, v2: 3)


//let array = [4,6,5,3,3,1]
//var value: [Int] = []
//for i in array {
//    var check = i
//    for j in 0...array.count-1 {
//        check = abs(check-array[j])
//        print(check)
//        if check < 2 {
//            value.append(array[j])
//        }
//    }
//}

//let array = [4,6,5,3,3,1]
//var value: [Int] = []
//for i in array {
//    var check = i
//    for j in 1...array.count-1 {
//        print(array[j])
//    }
//}


//let array = [4,6,5,3,3,1]
//var value: [Int] = []
//for i in array {
//    var check = i
//    for j in 0...array.count-1 {
//        check -= array[j]
//        print(array[j])
//    }
//}

//let array = [4,6,5,3,3,1]
//var value: [Int] = []
//let chunks = 2
//let newArray = stride(from: 0, to: array.count, by: 1).map {
//    Array(array[$0..<min($0+chunks,array.count)])
//}
//
//print(newArray)


//let array = [4,6,5,3,3,1]
//var value: [Int] = []
//var check = 1
//for i in 0...array.count-1 {
//    if array[i] - array[i+1] < 2 {
//        value.append(array[i])
//    }
//}

//let array = [4,6,5,3,3,1]
//var value: [Int] = []
//let chunks = 2
//let newArray = stride(from: 0, to: array.count, by: 1).map {
//    Array(array[$0..<min($0+chunks,array.count)])
//}
//
//print(newArray)

//let array = [4,6,5,3,3,1]
//var check = 1
//var maxim: [Int] = []
//for i in 0...array.count-1 {
//    var value: [Int] = []
//    for j in 0...array.count-1 {
//        if abs(array[i] - array[j]) < 2 {
//            value.append(array[j])
//        }
//    }
//    print(value)
//    maxim.append(value.count)
//}
//
//print(maxim.max()!-1)


//let array = [4,6,5,3,3,1]
//var maximA: [Int] = []
//var maximB: [Int] = []
//for i in 0...array.count-1 {
//    var valueA: [Int] = []
//    var valueB: [Int] = []
//    for j in 0...array.count-1 {
//        let sum = array[i] - array[j]
//        if sum < 2 {
//            if sum < 0 {
//                valueA.append(array[j])
//            } else {
//                valueB.append(array[j])
//            }
//        }
//
//    }
////    print(valueA)
////    print("-----")
//    print(valueB)
//    maximA.append(valueA.count)
//    maximB.append(valueB.count)
//}
//
////print(maximA.max()!)
//print(maximB.max()!)

//let date = Date()
//let format = DateFormatter()
//format.dateFormat = "dd MM yyyy"
//
//print(format.string(from: date))
//
//let calendar = Calendar.current
//
//print(calendar.component(.weekday, from: date))
//
////let month = Calendar.current.date(byAdding: .month, value: -2, to: date)
////calendar.component(.month, from: month!)
//
//let week = Calendar.current.date(byAdding: .day, value: -4, to: date)
//calendar.component(.day, from: week!)

//let interestingNumbers = ["primes": [2, 3, 5, 7, 11, 13, 17],
//"triangular": [1, 3, 6, 10, 15, 21, 28],
//"hexagonal": [1, 6, 15, 28, 45, 66, 91]]
//
//print(interestingNumbers["primes"]![1])

//func getDate(value: Int) -> Dictionary<String,[Int]> {
//    let date = Date()
//    let format = DateFormatter()
//    format.dateFormat = "dd MM yyyy"
//    format.dateStyle = .full
//    print(format.string(from: date))
//
//    let calendar = Calendar.current
//    let pastCalendar = Calendar.current.date(byAdding: .day, value: value, to: date)
//    let pastDate = [calendar.component(.day, from: pastCalendar!),
//                    calendar.component(.month, from: pastCalendar!),
//                    calendar.component(.year, from: pastCalendar!)]
//    let currentDate = [calendar.component(.day, from: date),
//                    calendar.component(.month, from: date),
//                    calendar.component(.year, from: date)]
//
//    let collectDate = ["Past": pastDate, "Now": currentDate]
//    return collectDate
//}
//
//let a = getDate(value: -2)
//print(a["Past"]!)


//func getDate(value: Int) -> (String, [Int]) {
//    let date = Date()
//    let format = DateFormatter()
//    format.dateFormat = "dd MM yyyy"
//
//    let calendar = Calendar.current
//    let pastCalendar = Calendar.current.date(byAdding: .day, value: value, to: date)
//    let pastDate = [calendar.component(.day, from: pastCalendar!),
//                    calendar.component(.month, from: pastCalendar!),
//                    calendar.component(.year, from: pastCalendar!)]
//    let currentDate = [calendar.component(.day, from: date),
//                    calendar.component(.month, from: date),
//                    calendar.component(.year, from: date)]
//
//    let collectDate1 = (state: "Past", date: pastDate)
//    let collectDate2 = (state: "Past", date: currentDate)
//
//    return collectDate1, collectDate2
//}
//
//getDate(value: -2)

//func getDate(value: Int, formatter: String) -> Dictionary<String,[String]> {
//    let date = Date()
//    let format = DateFormatter()
//    format.dateFormat = formatter
////    print(format.string(from: date))
//
//    let calendar = Calendar.current
//    let pastCalendar = Calendar.current.date(byAdding: .day, value: value, to: date)
//    let pastDate = [String(calendar.component(.day, from: pastCalendar!)),
//                    String(calendar.component(.month, from: pastCalendar!)),
//                    String(calendar.component(.year, from: pastCalendar!))]
//
//    let currentDate = [format.string(from: date)]
//
//    let collectDate = ["Past": pastDate, "Now": currentDate]
//    return collectDate
//}
//
//let a = getDate(value: -7, formatter: "dd MMMM yyyy")
//print(a["Past"]![1])
//let monthComponents = Calendar.current.shortMonthSymbols
//monthComponents[Int(a["Past"]![1])!-1]
//let day = Calendar.current.shorWeekdaySymbols


//func getDate(value: Int, formatter: String) -> Dictionary<String,[String]> {
////    let dateArray: [String] = []
//    let date = Date()
//    let format = DateFormatter()
//    format.dateFormat = formatter
//
//    let calendar = Calendar.current
//    let pastCalendar = Calendar.current.date(byAdding: .day, value: value, to: date)
//    let pastDate = [String(calendar.component(.day, from: pastCalendar!)),
//                    String(calendar.component(.month, from: pastCalendar!)),
//                    String(calendar.component(.year, from: pastCalendar!)),
//                    String(calendar.component(.weekday, from: pastCalendar!))]
//    let currentDate1 = [calendar.component(.day, from: date),
//                    calendar.component(.month, from: date),
//                    calendar.component(.year, from: date),
//                    calendar.component(.weekday, from: date)]
//
//    let currentDate = [format.string(from: date)]
//
//    print(currentDate1[0]-Int(pastDate[0])!)
////    for i in Int(pastDate[0])!...currentDate1[0]-1{
////
////    }
//
//    let collectDate = ["Past": pastDate, "Now": currentDate]
//    return collectDate
//}
//
//let a = getDate(value: -7, formatter: "dd MMMM yyyy")

//let dayComponents = Calendar.current.shortWeekdaySymbols
//let day = 4
//var dayArray: [String] = []
//
//for i in 0...day-1 {
////    print(day-1-i)
//    dayArray.append(dayComponents[day-1-i])
//}
//for i in day+1...dayComponents.count-1 {
////    print(dayComponents.count-i+day)
//    dayArray.append(dayComponents[dayComponents.count-i+day])
//}
//
//print(dayArray)

//let yearComponents = Calendar.current.veryShortMonthSymbols
//let day = 4
//var dayArray: [String] = []
//
//for i in 0...day-1 {
//    print(day-1-i)
//    dayArray.append(yearComponents[day-1-i])
//}
//for i in day+1...yearComponents.count-1 {
//    print(yearComponents.count-i+day)
//    dayArray.append(yearComponents[yearComponents.count-i+day])
//}
//
//print(dayArray.count)

//let a = Calendar.current.monthSymbols
//let b = Calendar.current.weekdaySymbols
//
//    func getDate(value: Int, formatter: String) -> Dictionary<String,[Int]> {
//        let date = Date()
//        let format = DateFormatter()
//        format.dateFormat = formatter
//
//        let calendar = Calendar.current
//        let pastCalendar = Calendar.current.date(byAdding: .day, value: value, to: date)
//        let pastDate = [calendar.component(.day, from: pastCalendar!),
//                        calendar.component(.month, from: pastCalendar!),
//                        calendar.component(.year, from: pastCalendar!)]
//
////        let currentDate = [format.string(from: date)]
//        let currentDate = [calendar.component(.day, from: date),
//                        calendar.component(.month, from: date),
//                        calendar.component(.year, from: date),
//                        calendar.component(.weekday, from: date)]
//
//        let collectDate = ["Past": pastDate, "Now": currentDate]
//        return collectDate
//    }
//
//let date = getDate(value: -30, formatter: "MMM yyyy")
//var currentMonth: [Int] = []
//var lastMonth: [Int] = []
//let a = date["Now"]![0]
//let b = 30 - a
//let c = date["Past"]![0]
//for i in 1...a {
//    currentMonth.append(i)
//}
//
//for i in c...c+b {
//    lastMonth.append(i)
//}
//
////print(currentMonth)
////print(lastMonth)
//
//let dataEntryY = [1,2,3,4,5,6,7,8,9,10,1,2,3,4,5,6,7,8,9,10,1,2,3,4,5,6,7,8,9]
//print(dataEntryY.count)
//
//
//var hours: [String] = [];
////for(var i=0; i < 24; i++) {
////    hours.push((i < 10 ? "0" : "") + i + ":00");
////}
//
//for i in 0...24 {
//    if i < 10 {
//        hours.append("0\(i)")
//    } else {
//        hours.append("\(i)")
//    }
//}
//print(hours)

//func generateRandomNumber (count: Int) -> [Int] {
//    var numbers: [Int] = []
//    for _ in 1...count {
//        numbers.append(Int.random(in: 40..<100))
//    }
//    return numbers
//}
//
//func average(numbers: [Int]) -> Int {
//    var number = 0
//    for i in numbers {
//        number += i
//    }
//    return number/numbers.count
//}
//
//let a = generateRandomNumber(count: 1000)
//print(a)
//average(numbers: a)

//let date = Date()
//print(date)

//func createDateTime(dateTimeString: String) -> Date {
//    let dateFormatter = DateFormatter()
//
//    dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
//
//    let date = dateFormatter.date(from: dateTimeString) ?? Date()
//
//    return date
//}

//let currentDate = Date()

//var dateComponent = DateComponents()
//dateComponent.day = -7

//let pastDate = Calendar.current.date(byAdding: dateComponent, to: currentDate)
//
//if pastDate! < currentDate {
//    print("Hai")
//}

//let array4 = [1, 3, 6, 18, 24]
//let array5 = [50, 100, 200]
//
//let hasNoCommonElement = Set(array4).isDisjoint(with: array5)
//print(hasNoCommonElement) // prints: true

//func generateRandomNumber(count: Int) -> [Int] {
//    var num: [Int] = []
//    for _ in 1...count {
//        num.append(Int.random(in: 1...10))
//    }
//    return num
//}
//
//let currentDate = Date()
//var dateComponent = DateComponents()
//
//// DB data
//var pastDate: [Date] = []
//for i in 1...4 {
//    dateComponent.day = -(i)
//    let date = Calendar.current.date(byAdding: dateComponent, to: currentDate)
//    pastDate.append(date!)
//}
//let trips = generateRandomNumber(count: 4)
//
//var dataBase:[(pastDate: Date, trip: Int)] = []
//for i in 0...pastDate.count-1 {
//    dataBase.append((pastDate[i],trips[i]))
//}
//var dataBaseDict = [Date:Int]()
//for i in 0...pastDate.count-1 {
//    dataBaseDict.updateValue(trips[i], forKey: pastDate[i])
//}
////print(dataBaseDict.keys)
//
//// Graph data
//var graphDate: [Date] = []
//for i in 1...7 {
//    dateComponent.day = -(i)
//    let date = Calendar.current.date(byAdding: dateComponent, to: currentDate)
//    graphDate.append(date!)
//}
//
//
//// Plot DB to Graph
//let intersection = Set(dataBaseDict.keys).intersection(graphDate)
////print(intersection)
//
//var score: [Int] = []
//
//// Dummy Dict
//var laporanDict = [String:Int]()
//for i in 1...4 {
//    laporanDict.updateValue(i*3, forKey: "Nilai ke\(i)")
//}
//print("Laporan: \(laporanDict)")
//
//var dbDict = [String:Int]()
//for i in 1...7 {
//    dbDict.updateValue(i, forKey: "Nilai ke\(i)")
//}
//print("Database: \(dbDict)")
//
//for i in laporanDict {
//    dbDict.updateValue(i.value, forKey: i.key)
//}
//print("Database update: \(dbDict)")


//let intersection2 = Set(laporanDict.keys).intersection(dbDict.keys)


//let array = [1,4,4,4,5,3]
//var count = 0
//var resultArray: [Int] = []
//
//func migratoryBirds(arr: [Int]) -> Int {
//    let maxArray = arr.max()
//    for i in 1...maxArray! {
//        for j in arr {
//            if i == j {
//                count += 1
//            }
//        }
//        resultArray.append(count)
//        count = 0
//    }
//    print(resultArray)
//
//    var max = 0
//    var posMax = 0
//    for i in 0...resultArray.count-1 {
//        if resultArray[i] > max {
//            max = resultArray[i]
//            posMax = i
//        }
//    }
//    print(posMax+1)
//
//    return posMax+1
//}
//
//migratoryBirds(arr: array)

//func bonAppetit(bill: [Int], k: Int, b: Int) -> Void {
//    var actualBill = 0
//    for i in 0...bill.count-1 {
//        if i != k {
//            actualBill += bill[i]
//        }
//    }
//    print(actualBill)
//    if actualBill/2 == b {
//        print("Bon Appetit")
//    } else {
//        print("\(b - actualBill/2)")
//    }
//}
//
//let k = 1
//let b = 7
//let array = [3,10,2,9]
//bonAppetit(bill: array, k: k, b: b)


//func sockMerchant(n: Int, ar: [Int]) -> Int {
//    var sortArray = ar
//    var count = 0
//    sortArray = sortArray.sorted()
//    print(sortArray.count)
//    for i in 0...(n/2)-1 {
//        print("genap \(2*i): \(sortArray[2*i])")
//        print("ganjil \(2*i+1): \(sortArray[2*i+1])")
//        if sortArray[2*i] == sortArray[2*i+1] {
//            count += 1
//        }
//    }
//    return count
//}

func sockMerchant(n: Int, ar: [Int]) -> Int {
    var countArray: [Int] = []
    for i in ar {
        for j in ar {
            print("\(i) : \(j)")
        }
    }
    
    return 1
}

let n = 100
let array3 = [44,55,11,15,4,72,26,91,80,14,43,78,70,75,36,83,78,91,17,17,54,65,60,21,58,98,87,45,75,97,81,18,51,43,84,54,66,10,44,45,23,38,22,44,65,9,78,42,100,94,58,5,11,69,26,20,19,64,64,93,60,96,10,10,39,94,15,4,3,10,1,77,48,74,20,12,83,97,5,82,43,15,86,5,35,63,24,53,27,87,45,38,34,7,48,24,100,14,80,54]
sockMerchant(n: n, ar: array3)


//let items = ["a", "b", "a", "c"]
//
//let mappedItem = items.map { ($0,1) }
//print(mappedItem)
//
//let countItem = Dictionary(mappedItem, uniquingKeysWith: +)

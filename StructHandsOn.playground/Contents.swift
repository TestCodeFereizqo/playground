import UIKit

// Create struct for color
struct Color {
    let red: Double
    let green: Double
    let blue: Double
    
    // Create function to tell color name
    func tellColorName() {
        if red == 1 && green == 0 && blue == 0 {
            print("The color is red")
        } else if red == 0 && green == 0 && blue == 1 {
            print("The color is blue")
        } else {
            print("The color is yellow")
        }
    }
}

// Create variable from Color struct
let redColor: Color = Color(red: 1, green: 0, blue: 0)
let blueColor = Color(red: 0, green: 0, blue: 1)
let yellowColor = Color(red: 1.0, green: 1.0, blue: 0.0)


// Create struct for size
struct Size {
    let width: Int
    let height: Int
    let length: Int
}

// Create variable from Size struct
let smallSize: Size = Size(width: 130, height: 140, length: 300)
let mediumSize = Size(width: 150, height: 160, length: 420)
let largeSize = Size(width: 170, height: 200, length: 500)

// Create struct for car
struct Car {
    var name: String
    let brand: String
    let color: Color
    let dimension: Size
    
    // Create function to print car description
    func tellMyCar() {
        print("My car is \(name) and the brand is \(brand)")
        color.tellColorName()
    }
}

// Create variable for cars
let myCar1: Car = Car(name: "Brio", brand: "Honda", color: yellowColor, dimension: smallSize)
var myCar2 = Car(name: "Rush", brand: "Toyota", color: Color(red: 0, green: 0, blue: 1), dimension: mediumSize)
let myCar3 = Car(name: "Pajero", brand: "Mitsubishi", color: redColor, dimension: Size(width: 170, height: 200, length: 500))


// Accessing property from struct
myCar1.name
myCar1.color
myCar1.color.red

print(myCar1.name)
print(myCar1.color)
print(myCar1.color.red)
print(myCar1)

myCar1.tellMyCar()

myCar2.name = "Fortuner"
myCar2.tellMyCar()

/* MINI EXERCISE
 1. Add new function to tell car dimension is small / medium / large
 2. Change car color (red / green / blue)
*/

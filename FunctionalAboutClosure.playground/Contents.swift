import Cocoa
import SwiftUI

// Part 1

/// Defining Closure

func addTwoNumbers(num1: Int, num2: Int) -> Int {
    return num1+num2
}
addTwoNumbers(num1: 10, num2: 20)

let closure: (Int, Int) -> Int = { (number1, number2) in
    return number1+number2
}
closure(10,20)

let shortHandClosure: (Int, Int) -> Int = {
    return $0+$1
}
shortHandClosure(10,20)

let superShortHandClosure: (Int,Int) -> Int = {$0 + $1}


/// Inferred Type Closure

let inferredClosure = { (num1: Int, num2: Int) -> Int in num1 + num2}
inferredClosure(10,20)

let inferredReturnTypeClosure =  { (number:Int) in number+number }
inferredReturnTypeClosure(10)


/// Closure takes nothing and return a string

let callStringWithClosure: () -> String = { () in
    return "Hello"
}
print(callStringWithClosure())

let callStringOmit: () -> String = { return "Hello" }

let callStringWithClosureWithoutType = { "Hello" }
let callStringWithClosureWithoutTypess = { "Hello" }()
let varString = "Hello"

print(callStringWithClosureWithoutType) // Tipe variablenya: () -> String
print(callStringWithClosureWithoutTypess) // Tipe variablenya: String

/// Returning a Closure from Function

var addClosure: (Int,Int) -> Int = { $0+$1 }
func returnClosure() -> (Int,Int) -> Int {
    return addClosure
}
returnClosure()(10,20)

var returnedClosure = returnClosure()
returnedClosure(10,20)


/// Intitialize or Customize Object using Closure

// Read in project "FunctionalAboutClosure" because using UIKit




// Part 2

/// Trailing Closures

func doSomething (number: Int, onSucces closure: (Int)-> Void) {
    closure(number + number + number)
}

doSomething(number: 100) { (resultNumber) in
    print(resultNumber)
}


/// Capturing Values
// Swift handles all of the memory management of capturing for you.
var count1 = 0
var closureArray1 = [() -> ()]()

for _ in 1...5 {
    closureArray1.append {
        print(count1)
    }
    count1 += 1
}

// the result will be 5
closureArray1[1]()
closureArray1[2]()


/// Capturing List
var count2 = 0
var closureArray2 = [() -> ()]()

for _ in 1...5 {
    closureArray2.append { [count2] in
        print(count2)
    }
    count2 += 1
}

// the result will be increment
closureArray2[1]()
closureArray2[2]()


/// Escaping Closure vs. Non-escaping Closure
///     Escaping closure         : is a closure that's called "after" the function it was passed to return
///     (used in when the code is executed in the future, i.e.: completion handles, asychronous programming)
///     Non-escaping closure : is a closure that's caleed "within" the function it was passed into. i.e. before it returns
///     (default Swfit 3 above -> non-escaping closure)

func someFunctionWithNoneEscapingClosure(closure: () -> Void) {
    closure()
}

func someFunctionWithEscapingClosure(completionHandler: @escaping() -> Void ) {
    completionHandler()
}

class SomeClass {
    var x = 10
    
    func doSomething() {
        
        someFunctionWithEscapingClosure {
            self.x = 100
        }
        
        someFunctionWithNoneEscapingClosure {
            x = 100
        }
    }
}



// Part 3

/// Memory Management

/// Strong reference cycle occurs when two objects keep a strong reference of each other. Because of this cycle, the two objects won’t be deallocated, since their reference count doesn’t drop to zero. example: name and greetings in this class
class InterviewTest {
    var name: String = "Abhilash"
    lazy var greeting: String = {
        return "Hello \(self.name)"
    }()
}

let testObj = InterviewTest()
testObj.greeting


/// We can break this strong reference cycle using self in the capture list with either a weak or an unowned reference.

/// Weak
/// Since a weak reference may be nil, the variable captured becomes optional. Therefore, we should use a guard to safely unwrap it.

class InterviewTestWeak {
    var name: String = "Abhilash"
    lazy var greeting: String = { [weak self] in
        guard let strongSelf = self else {return ""}
        return "Hello \(strongSelf.name)"
    }()
    
    var firstName: String = ""
    var lastName: String = ""
    
    lazy var fullName: () -> String = { [weak self] in
        guard let weakSelf = self else { return "" }
        return "\(weakSelf.firstName) \(weakSelf.lastName)"
    }
}

/// Unowned
/// an unowned reference is used when the other instance has the same lifetime or a longer lifetime.
/// This means that we should use unowned just when we are sure that the reference will never be nilinside the closure, otherwise the app would crash.

class InterviewTestUnowned {
    var name: String = "Abhilash"
    lazy var greeting: String = { [unowned self] in
        return "Hello \(self.name)"
    }()
    
    var firstName: String = ""
    var lastName: String = ""
    
    lazy var fullName : () -> String = { [unowned self] in
        return "\(self.firstName) \(self.lastName)"
    }
    
}

/// we can use aliases either in weak or unowned
class InterviewTestAliases {
    var name: String = "Abhilash"
    lazy var greetings: String = { [unowned unownedSelf = self] in
        return "Hello, \(unownedSelf.name)"
    }()
}

/// Capture self with closures with argument

class Human {
    var firstName: String = ""
    var lastName: String = ""
    
//    lazy var fullName: (String) -> String = { [weak self] titlePrefix in
//        guard let strongSelf = self else { return ""}
//        return "\(titlePrefix). \(strongSelf.firstName) \(strongSelf.lastName)"
//    }
    
    lazy var fullName: (String) -> String = { [unowned self] titlePrefix in
        return "\(titlePrefix). \(self.firstName) \(self.lastName)"
    }
    
    init(firstName: String, lastName: String) {
        self.firstName = firstName
        self.lastName = lastName
    }
    
    deinit {
        print("De-allocating the Human object named \(firstName) \(lastName)")
    }
}

var humanObj = Human(firstName: "John", lastName: "Doe")
let fullName = humanObj.fullName("Mr")
//humanObj = nil

/// Exeptional cases:
/// Although it is good practice to capture self weakly in closures, it is not always necessary.
/// Closures that are not retained by the self can capture it strongly without causing a retain cycle.
/// i.e. DispatchQueue, animation

/*
DispatchQueue.main.async {
    self.doSomething()      // Not a retain cycle
}

UIView.animate(withDuration: 1) {
    self.doSomething()      // Not a retain cycle
}
*/

/// Another interesting place where self would not need to be captured strongly is in lazy variables, that are not closures
/// since they will be run once (or never) and then released afterwards:
/*
 lazy var fullName = {
   return self.firstName + " " + self.lastName
 }() // not retained. no need to use self with weak or unowned
 */


///However, if a lazy variable is a closure, it would need to capture self weakly/unowned.
///The fullName:()->String or fullName:(String) -> String closure in the Human class is a good example for this.



/// Autoclosures
/// to make code more readable

var customerInLine = ["Chris", "Alex", "Ewa", "Barry", "Daniella"]

/// automatic detect as autoclosure
let customerProvider = { customerInLine.remove(at: 0) }
print(customerProvider())


/// compare to passed on to a function as argument (explicit closure)
func serve(customer customerProviders: () -> String) {
    print("Now serving \(customerProviders())")
}
/// customersInLine is ["Alex", "Ewa", "Barry", "Daniella"]
serve(customer: {customerInLine.remove(at: 0)} )


/// parameter's type is marked with autoclosure attribute
func serveAutoAtt(customer customerProviders: @autoclosure () -> String) {
    print("Now serving \(customerProviders())")
}
/// customersInLine is ["Ewa", "Barry", "Daniella"]
serveAutoAtt(customer: customerInLine.remove(at: 0))

/// the difference autoclosure or explicit closure is the Bracket



/// Combine Autoclosure with Escaping, example :
/*
 
 extension UIView {

 class func animate(withDuration duration: TimeInterval, _ animations: @escaping @autoclosure () -> Void) {
         UIView.animate(withDuration: duration, animations: animations)
     }

 }
 
 /// we can call it :
 UIView.animate(withDuration: 2.5) {
     self.view.backgroundColor = .orange
 }
 
 */





/// Functions and closures are reference types.
/// Here , in the following code, both addClosure2 and addClosure refers to the same closure in memory.

var addClosures:(Int,Int)->Int = { $0 + $1 }
let addClosures2 = addClosures

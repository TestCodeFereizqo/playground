import Cocoa

/*
 Higher Order Function are
 a. functions that take another function / closure as argument and returns it
 b. return function from another function
*/


/// a. Function that take another function

func addition(num1: Double, num2: Double) -> Double {
    return num1 + num2
}

func multiply(num1: Double, num2: Double) -> Double {
    return num1 * num2
}

func doMathOperation(operation: (_ x: Double, _ y: Double)-> Double, num1: Double, num2: Double) -> Double {
    return operation(num1,num2)
}

//doMathOperation(operation: <#T##(Double, Double) -> Double#>, num1: <#T##Double#>, num2: <#T##Double#>)
doMathOperation(operation: addition(num1:num2:), num1: 20, num2: 20)
doMathOperation(operation: multiply, num1: 10, num2: 10)


/// b. Return function from another function

func doArithmeticOperation(isMultiply: Bool) -> (Double,Double) -> Double {
    func additionArit(num1: Double, num2: Double) -> Double {
        return num1+num2
    }
    func multiplyArit(num1: Double, num2: Double) -> Double {
        return num1*num2
    }
    
    return isMultiply ? multiplyArit : additionArit
}

let arit1 = doArithmeticOperation(isMultiply: true)
arit1(10,10)

func doOperation(oprator: String) -> ((Double, Double) -> Double)? {
    if oprator == "+" {
        return (+)
    } else if oprator == "-" {
        return (-)
    } else if oprator == "*" {
        return (*)
    } else if oprator == "/" {
        return (/)
    } else if oprator == "**" {
        return pow
    } else {
        return nil
    }
}



// Saved function into variable
let test = addition(num1: 10, num2: 10)
let test2 = doArithmeticOperation(isMultiply: false)
test2(10,10)

import UIKit

let name = ("John","Allen")
let fisrtName = name.0
let lastName = name.1

let nameId = (name: "John", id: 1)
let namee = nameId.name
let id = nameId.id

let (a,b) = nameId
print("This is your name \(a) with id \(b)")


let bar: (Int, (Bool, String)) = (1,(true, "Hello"))
print(bar.0)
print(bar.1)
print(bar.1.0)
print(bar.1.1)

var x = 1
var y = 2
(x,y) = (y,x)
print(x,y)

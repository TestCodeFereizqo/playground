import UIKit

var str = "Hello, my name is Yulibar Samuel"

print (str)

// For Loop
for index in 0...9 {
    print(str + (" \(index)"))
}

for index in 0..<9 {
    print("\(index)")
    print(index)
}


// While Loop
var rekeningFilbert = 2000
var gajiFilbert = 1000000000
var hargaLambo = 16000000000

while rekeningFilbert < hargaLambo {
    rekeningFilbert = rekeningFilbert + gajiFilbert
    print("Rekening Filbert sekarang : IDR \(rekeningFilbert)")
}

var index = 0
while index < 9 {
    print (index)
    index = index + 1
}

// Array Loop
var names = ["Dewi","Audi","Dhiena"]
    // Variable "i" only exist within this bracket
for i in names{
    print ("Hey, \(i)")
}
// Variable "i" does not exist below here
// name


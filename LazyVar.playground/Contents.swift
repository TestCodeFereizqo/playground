import Cocoa

// Lazy property :
/// - lazy stored property is a property that is not calculated until the first time it is used
/// - must declared  with var keyword, because
///    - its initial value might not be retrieved after instance initialization completes
///    - let (constant) property must always have a value before

struct interviewCandidate {
    var isiOS: Bool?
    
    lazy var iOSResumeDescription: String = {
        return "I am an iOS Developer"
    }()
    
    lazy var androidResumeDescription: String = {
        return "I am an android developer"
    }()
    
    var name = "Developer"
    
    func test() {
        print("Just to test")
    }
}

var person1 = interviewCandidate()
person1.isiOS = true
person1.test()

if person1.isiOS! {
    print(person1.iOSResumeDescription)
} else {
    print(person1.androidResumeDescription)
}

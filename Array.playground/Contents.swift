import UIKit


//var Ingredients = ["Rice","Egg","Meat","Onion"]
//var Ingredients: [String] = ["Rice","Egg","Meat","Onion"]
//var Prices: [Int] = [500,1000,1200,5000]

//print(Ingredients)
//print(Ingredients[0])

//print("There are \(Ingredients.count) items in the array")

//Ingredients.remove(at: 2)
//print(Ingredients)

//Ingredients.removeAll()
//print(Ingredients)


//for i in 0...Ingredients.count-1{
//    print("The price of \(Ingredients[i]) is \(Prices[i])")
//}

//var stockCode = ["APPL","MFST","BABA","TSLA","NVDA"]
//var stockValues = [203.86,123.37,186.94,273.26,186.30]
//
//stockCode.append("TWTR")
//stockValues.append(34.40)
//
//stockCode.remove(at: 1)
//stockValues.remove(at: 1)
//
//print("Result:")
//print("The first stock is \(stockCode[0]) valued \(stockValues[0])")
//
//for i in 0...stockCode.count-1 {
//    print("Stock \(stockCode[i]) is valued $\(stockValues[i])")
//}


//let a = "2020-02-21T03:21:51.919729Z"

//let arrayClock = a.components(separatedBy: "T")
//let dateClock = arrayClock[0]
//let timeClock = arrayClock[1]
//
//let date = dateClock.components(separatedBy: "-")
//let year = Int(date[0])
//let month = Int(date[1])
//let day = Int(date[2])
//
//let time = timeClock.components(separatedBy: ":")
//let hour = Int(time[0])
//let minute = Int(time[1])
//
//func makeDate(year: Int, month: Int, day: Int, hr: Int, min: Int) -> Date {
//    var calendar = Calendar(identifier: .gregorian)
//     calendar.timeZone = TimeZone(secondsFromGMT: 7)!
//    let components = DateComponents(year: year, month: month, day: day, hour: hr, minute: min)
//    return calendar.date(from: components)!
//}
//
//let dates = makeDate(year: year!, month: month!, day: day!, hr: hour!, min: minute!)
//
//let calendar = Calendar.current
//let hourss = calendar.component(.hour, from: dates)
//let minutesss = calendar.component(.minute, from: dates)


let input = "2020-02-21T03:21:51.919729Z"

let formatter = DateFormatter()
formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSSSSZ"
if let date = formatter.date(from: input) {
    print("date: \(date)")
    
    let formatter = DateFormatter()
    formatter.dateFormat = "hh:mm a"
    formatter.timeZone = TimeZone(abbreviation: "UTC+7")
    let time = formatter.string(from: date)
}

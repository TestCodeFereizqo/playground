import UIKit

struct DaftarMenu {
    var menu: String
    var quantity: Int
    var price: Int
}

var OrderedItem = [DaftarMenu] ()

var order1 = DaftarMenu(menu: "Ayam", quantity: 2, price: 13000)
OrderedItem.append(order1)
order1 = DaftarMenu(menu: "Terong", quantity: 3, price: 8000)
OrderedItem.append(order1)
order1 = DaftarMenu(menu: "Bebek", quantity: 4, price: 18000)
OrderedItem.append(order1)
order1 = DaftarMenu(menu: "Gurami", quantity: 2, price: 15000)
OrderedItem.append(order1)
order1 = DaftarMenu(menu: "Hati", quantity: 2, price: 10000)
OrderedItem.append(order1)

print("\t .:Penyetan Bang Ari:.")
print("\t ---------------------")
print("\t Item Ordered: \(OrderedItem.count) items")
print("\t ---------------------\n")

var TotalPrice = 0
for order in OrderedItem {
    print("\(order.menu) \t x\(order.quantity) @\(order.price) \t Rp \(order.price*order.quantity)")
    TotalPrice += order.price*order.quantity
}

print("-----------------------------")
print("-----------------------------")
print("TOTAL NEEED TO BE PAID:")
print("\t \t \t \t \tRp \(TotalPrice)")



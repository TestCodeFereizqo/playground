import UIKit

// Create color struct
struct Color {
    let red : Double
    let green : Double
    let blue : Double
    
    func tellColorName() {
        if red == 1 && green == 0 && blue == 0 {
            print("The color is red")
        } else if red == 0 && green == 1 && blue == 0{
            print("The color is green")
        } else {
            print("The color is blue")
        }
    }
}

let a : String = String("ada")
let b = String("fasf")

//Create variable from Color struct
let merah : Color = Color(red: 1, green: 0, blue: 0)
let hijau = Color(red: 0, green: 1, blue: 0)
let kuning = Color(red: 1, green: 1, blue: 0)

// Create Size struct
struct Size {
    let length : Int
    let heigth : Int
    let width : Int
}

//Create varible from Size struct
let smallSize = Size(length: 200, heigth: 100, width: 100)
let mediumSize = Size(length: 310, heigth: 130, width: 150)
let largeSize = Size(length: 440, heigth: 180, width: 210)

//Create struct for Car
struct Car {
    let name : String
    let brand : String
    let color : Color
    let dimension : Size
    
    func tellMyCar() -> String {
        //print("My car is \(name) from \(brand)")
        let a = "My car is \(name) from \(brand)"
        return a
    }
}

//Create variable for cars
let myCar1 : Car = Car(name: "Brio", brand: "Honda", color: kuning, dimension: smallSize)
let myCar2 = Car(name: "Rush", brand: "Toyota", color: Color(red: 0, green: 0, blue: 1), dimension: mediumSize)
let myCar3 = Car(name: "Pajero", brand: "Mitsubishi", color: merah, dimension: Size(length: 450, heigth: 200, width: 220))

myCar1.name
myCar1.brand
myCar1.color

myCar1.tellMyCar()
merah.tellColorName()
